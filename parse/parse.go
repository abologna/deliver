// SPDX-License-Identifier: GPL-3.0-or-later
//
// (c) 2019 Red Hat

package parse

import (
    "os"
    "bufio"
    "regexp"
)

func ParseCode() (code string) {
        var scanner = bufio.NewScanner(os.Stdin)
        var sixDigits = regexp.MustCompile(`>([0-9]{6})<`)

        for scanner.Scan() {
                var text = scanner.Text()

                if sixDigits.MatchString(text) {
                        return string(sixDigits.FindSubmatch([]byte(text))[1])
                }
        }
        return
}
